Sende Texte (siehe Parameter) an einen oder mehrere RocketChat Channels oder setze einfach nur den Status.


# Vorraussetzung

Ich empfehle die Verwendung von [rbenv](https://github.com/rbenv/rbenv) um die richtige Ruby Version zu verwenden.
Alternativ das ganze als Docker-Script laufen zu lassen.

# Installation/Vorbereitung (ohne Docker)

1. Ruby Abhängigkeiten installieren `bundle`
2. Eine `.env` Datei anlegen (oder die `_.env` Vorlage verwenden - umbenennen)

# Installation mit Docker

1. Docker Build `docker build .`
2. Eine `.env` Datei anlegen (oder die `_.env` Vorlage verwenden - umbenennen)

# API-Zugang zu RocketChat bekommen

1. Im RocketChat im eigenen Profil unter `Mein Konto` > `Persönlicher Zugangsschlüssel` einen neuen Zugangsschlüssel ausstellen. (Zwei-Faktor-Authentifikation ignorieren)
2. Die API-Werte in die `.env`-Datei eintragen
3. Die Channels einfügen, in die geschrieben werden soll. **Das '#' am Anfang des Channels darf nicht mit geschrieben werden.** Bei mehr als einem Channel, diese mit Leerzeichen trennen aber in `"` setzen


# Ausführen

## Ausführen als Script ohne Docker

`ruby say.rb PARAMETER` bzw. `bundle exec ruby say.rb PARAMETER`

## Ausführen als Script ohne Docker

`docker run --env-file .env rcmessage PARAMETER`

## Parameter

### hello

Schreibt in Channel(s): Guten Morgen
Setzt Status: Online (Text: Online)

### bye

Schreibt in Channel(s): Ich bin dann mal weg
Setzt Status: Offline (Text: Offline)

### pause
    
Schreibt in Channel(s): Ich mach dann mal Pause
Setzt Status: Away (Text: Pause)

### return

Schreibt in Channel(s): Bin wieder da
Setzt Status: Online (Text: Online)

### busy

Schreibt in Channel(s): _nichts_
Setzt Status: Busy (Text: DND)

### online

Schreibt in Channel(s): _nichts_
Setzt Status: Online (Text: Online)

# Bonus

## Alfred Workflow

Mein Alfred Workflow findest du unter `/alfres_workflow`