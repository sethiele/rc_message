#!/usr/local/bin/ruby -w

# Nutzung auf eigene Gefahr

require 'net/http'
require 'uri'
require 'json'
require 'dotenv'

Dotenv.load(File.join(File.dirname(__FILE__), '.env'))

##### Konfiguration ####
$config = {}
$config[:rc_user_id] = ENV['RC_USER_ID']
$config[:rc_auth_token] = ENV['RC_AUTH_TOKEN']
$config[:rc_channels] = ENV['RC_CHANNELS']
$config[:rc_server] = ENV['RC_SERVER']

def send_to_rc(message, endpoint)
    uri = URI.parse($config[:rc_server] + "/api/v1/" + endpoint)
    request = Net::HTTP::Post.new(uri)
    request.content_type = "application/json"
    request["X-Auth-Token"] = $config[:rc_auth_token]
    request["X-User-Id"] = $config[:rc_user_id]
    request.body = JSON.dump(message)
    req_options = {
        use_ssl: uri.scheme == "https",
    }

    response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
        http.request(request)
    end
    if response.code != "200"
        raise RuntimeError.new(
        "Rocketchat Returns Code #{response.code} - #{response.body}"
    )
    end
    #puts response.code
    #puts response.body
end

def per_channel(text)
    channels = $config[:rc_channels].split(" ")
    channels.each do |c|
        send_to_rc({"channel" => "#" + c, "text" => text}, "chat.postMessage")
    end
end

if ARGV[0].nil?
    raise ArgumentError.new(
        "Argument Error. Expected 1 got 0"
    )
elsif ARGV[0].downcase == "hello"
    per_channel("Guten Morgen")
    send_to_rc({"message" => "Online", "status" => "online"}, "users.setStatus")
elsif ARGV[0].downcase == "bye"
    per_channel("Ich bin dann mal weg")
    send_to_rc({"message" => "Offline", "status" => "offline"}, "users.setStatus")
elsif ARGV[0].downcase == "pause"
    per_channel("Ich mach dann mal Pause")
    send_to_rc({"message" => "Pause", "status" => "away"}, "users.setStatus")
elsif ARGV[0].downcase == "return"
    per_channel("Bin wieder da")
    send_to_rc({"message" => "Online", "status" => "online"}, "users.setStatus")
elsif ARGV[0].downcase == "busy"
    send_to_rc({"message" => "DND", "status" => "busy"}, "users.setStatus")
elsif ARGV[0].downcase == "online"
    send_to_rc({"message" => "Online", "status" => "online"}, "users.setStatus")
else
  raise ArgumentError.new(
    "Unknown Argument #{ARGV[0]}"
  )
end